FROM node:slim

WORKDIR /app
ADD /package.json /app/package.json
RUN npm install

RUN npm install -g pm2

ADD /src /app

ENTRYPOINT pm2-docker --raw start Startup.js