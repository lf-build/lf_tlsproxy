"use strict"

var bunyan = require("bunyan");
var http = require("request");
var Restify = require("restify");

var RequestLogger = require("./RequestLogger");


class Startup {

    constructor() {

        this.services = {
            levelStringToStream :function() {
                return {
                    write: log => {
            
                        // Map int log level to string
                        const clonedLog = Object.assign({}, log, {
                            level: bunyan.nameFromLevel[log.level]
                        });
            
                        var logLine = JSON.stringify(clonedLog, bunyan.safeCycles()) + '\n';
                        process.stdout.write(logLine);
                    }
                };
            },
            
            logger : function(){
                return bunyan.createLogger({
                    name: 'tlsproxy',
                    service:'tlsproxy',
                    streams: [{
                        type: 'raw',
                        stream: this.levelStringToStream()
                      }]
                });
            },
            requestLogger: function () {
                return new RequestLogger(this.logger());
            }
        }
    }

    createRoutes(server) {
        var self = this;

        var pattern = /(.*)/;
        var handle = function (request, response, next) {

            if(request.headers.host == undefined){
                response.send(400, new Error("Missing Host header"));
                return;
            }

            var target = "https://" + request.headers.host + request.path();

            var query = request.getQuery();
            if(query != undefined && typeof query === "string" && query.length > 0)
                target = target + "?" + query;
            
            var options = {
                url : target,
                forever : true
            };

            if(request.headers["x-proxy-certificate"]!=undefined){
                var certificate= Buffer.from(request.headers["x-proxy-certificate"], 'base64');
	            delete request.headers["x-proxy-certificate"];

                var password='';

                if(request.headers["x-proxy-passphrase"]!=undefined){
	                password=Buffer.from(request.headers["x-proxy-passphrase"], 'base64');
	                delete request.headers["x-proxy-passphrase"];
                }

                options.agentOptions = {
                    pfx: certificate,
                    passphrase: password,
                    securityOptions: 'SSL_OP_NO_SSLv3'
                }
            }

            request
                .pipe(http(options))
                .pipe(response);
        };
       
        server.put(pattern, handle);
        server.post(pattern, handle);
        server.del(pattern, handle);
        server.get(pattern, handle);
        server.head(pattern, handle);
    }

    run() {

        var self = this;
        
        var server = Restify.createServer();
       
        server.use(function (request, response, next) {
            var log = self.services.requestLogger();
            try {
                next.call(request, response);
            }
            catch (error) {
                log.error(request, response, error);
                return;
            }
            log.info(request, response);
        });


        this.createRoutes(server);

        server.listen(5000, function () {
            self.services.logger().info("server started");
        });

    }
}

new Startup().run();